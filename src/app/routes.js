import IndexPage from './pages/IndexPage'
import AboutPage from './pages/AboutPage'

export default [{
        path: "/",
        page: IndexPage,
    },
    {
        path: '/about',
        page: AboutPage,
    }
];
