import React from 'react'
import PropTypes from 'prop-types'
import {
    Switch,
    Route
} from 'react-router-dom'
import {
    Provider
} from 'mobx-react'
import routes from '../routes'
import NotFoundPage from './NotFoundPage'

class Layout extends React.Component {
    render() {
        return (
            <Provider {...this.props} >
                 <Switch>
                      {routes.map((route, i) => {
                           return <Route key={i} exact path={route.path} component={route.page}/>
                      })}
                      <Route component={NotFoundPage}/>
                 </Switch>
            </Provider>
        )
    }
}

Layout.propTypes = {
    store: PropTypes.object.isRequired,
    state: PropTypes.object.isRequired,
}

export default Layout
