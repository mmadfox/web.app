import React from 'react'
import {
    renderToStaticMarkup
} from 'react-dom/server'
import fs from 'fs'
import {
    resolve
} from 'path'
import {
    matchRoutes
} from 'react-router-config'
import Layout from '../../app/pages/Layout'
import {
    StaticRouter
} from 'react-router-dom'
import routes from '../../app/routes'

const html = fs.readFileSync(resolve(__dirname, '../index.html'), 'utf8')

export default async (ctx, resp, next) => {
    const context = {}
    const mr = matchRoutes(routes, ctx.url)
    const promises = mr.map(({
        route,
        match
    }) => {
        return route.page.onEnter ?
            route.page.onEnter(ctx.context, match.params) :
            Promise.resolve(null)
    })
    await Promise.all(promises)

    const components = renderToStaticMarkup(
        <StaticRouter location={ctx.url} context={context}>
            <Layout {...ctx.context}/>
         </StaticRouter>
    )

    if (context.url) {
        ctx.redirect(context.url)
        ctx.body = '<!DOCTYPE html>redirecting'
        return await next()
    }

    const chtml = html
        .replace('{title}', 'UpWinGO')
        .replace('{state}', JSON.stringify(ctx.context.state, null, 2))
        .replace('{children}', components)

    resp.send(chtml)
};
