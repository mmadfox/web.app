const webpack = require('webpack');
const webpackConf = require('../webpack');
const env = process.env.NODE_ENV === 'production' ?
    'production' :
    'development';

webpackConf.forEach((factory) => {
    var config = factory(env);
    const compiler = webpack(config, (err, stats) => {
        if (err != null) {
            return console.error(err);
        }
    });
});
